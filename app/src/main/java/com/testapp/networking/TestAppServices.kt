package com.testapp.networking

import com.testapp.networking.models.WsComment
import com.testapp.networking.models.WsPost
import retrofit2.http.GET
import retrofit2.http.Path

interface TestAppServices {

    @GET("users/{user}/posts")
    suspend fun getPosts(@Path("user") user: Long): List<WsPost>

    @GET("posts/{user}/comments")
    suspend fun getComments(@Path("user") user: Long): List<WsComment>
}