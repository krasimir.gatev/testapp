package com.testapp.networking

import android.os.SystemClock
import okhttp3.Interceptor
import okhttp3.Response

class AuthInterceptor() : Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response {
        var req = chain.request()
        //Simulate server side delay
        SystemClock.sleep(2000)

        // Add Authorization code here
        val url = req.url().newBuilder().addQueryParameter("APPID", "your_key_here").build()
        req = req.newBuilder().url(url).build()
        return chain.proceed(req)
    }
}