package com.testapp.networking.models

data class WsComment(val postId: Long, val id: Long, val name: String, val email: String, val body:String)