package com.testapp.networking.models

data class WsPost(val userId: Long, val id: Long, val title: String, val body: String)