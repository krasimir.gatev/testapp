package com.testapp.domain.models

data class DomainComment(val postId: Long, val id: Long, val name: String, val email: String, val body:String){

}