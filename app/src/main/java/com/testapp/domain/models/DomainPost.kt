package com.testapp.domain.models

data class DomainPost(
    val comments: List<DomainComment>,
    val userId: Long,
    val id: Long,
    val title: String,
    val body: String
)