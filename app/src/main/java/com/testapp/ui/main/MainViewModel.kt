package com.testapp.ui.main

import androidx.lifecycle.*
import com.testapp.db.User
import com.testapp.db.UserDao
import com.testapp.domain.models.DomainPost
import com.testapp.repository.ForumRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import org.koin.android.ext.android.inject
import org.koin.dsl.module
import kotlin.system.measureTimeMillis

val mainViewModelModule = module {
    factory { MainViewModel(get(), get()) }
}

class MainViewModel(
    private val forumRepo: ForumRepository,  private val userDao: UserDao): ViewModel() {

    val postsSequential =  MutableLiveData<List<DomainPost>>()
    val executionTime = MutableLiveData<Long>()
    val isLoadingInProgress = MutableLiveData<Boolean>()

    var postsParallel: LiveData<List<DomainPost>> = liveData {
        //Create user for test purposes
        val user = User(1, 1, "Test", "Name")
        userDao.insertAll(user)

        //Emit value and then wait 10 seconds until live data stopped being observed
        //while(true) is ok here, as this is restricted to the livedata's life cycle
        //or will stop when it is no longer observed
        while(true) {
            isLoadingInProgress.value = true
            executionTime.value = measureTimeMillis {

                //Emit the result of suspend function to the live data
                 emit(forumRepo.getDomainPostsParallel())
            }
            isLoadingInProgress.value = false

            delay(10_000)
        }
    }

    fun loadPosts(){
        viewModelScope.launch {
            //Create user for test purposes
            val user = User(1, 1, "Test", "Name")
            userDao.insertAll(user)

            isLoadingInProgress.value = true
            executionTime.value = measureTimeMillis {

                //Set the value of the suspend function to posts
                val wsPosts: List<DomainPost> = forumRepo.getDomainPostsSequential()
                postsSequential.value = wsPosts
            }
            isLoadingInProgress.value = false
        }
    }

}
