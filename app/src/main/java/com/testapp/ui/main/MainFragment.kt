package com.testapp.ui.main

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.liveData
import androidx.lifecycle.observe
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.testapp.domain.models.DomainPost
import com.testapp.ui.adapters.PostsAdapter
import kotlinx.android.synthetic.main.main_fragment.*
import kotlinx.android.synthetic.main.main_fragment.view.*
import org.koin.androidx.viewmodel.ext.android.viewModel


class MainFragment : Fragment() {

    companion object {
        fun newInstance() = MainFragment()
    }

    private lateinit var recyclerView: RecyclerView
    private lateinit var viewAdapter: PostsAdapter
    private lateinit var viewManager: RecyclerView.LayoutManager

    private var progressView: View? = null

    //View model is "injected" here
    private val mainViewModel: MainViewModel by viewModel()

    private val observerSequentialPosts = Observer<List<DomainPost>> {
        viewAdapter.setItems(it)
    }

    private val observerParallelPosts = Observer<List<DomainPost>> {
        viewAdapter.setItems(it)
    }

    private val observerExecutionTime = Observer<Long> { time ->
        context?.let { context ->
            mainViewModel.isLoadingInProgress.value?.let {
                if (it) {
                    Toast.makeText(context, "Execution Time: $time ms", Toast.LENGTH_LONG).show()
                }
            }
        }
    }

    private val observerLoadingProgress = Observer<Boolean> { isInProgress ->
        progressView?.let {
            if (isInProgress) {
                viewAdapter.setItems(listOf())
                it.visibility = View.VISIBLE
            } else {
                it.visibility = View.GONE
            }
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val rootView = inflater.inflate(com.testapp.R.layout.main_fragment, container, false)
        progressView = rootView.progress_view

        context?.let { context ->
            val posts = mutableListOf<DomainPost>()
            viewManager = LinearLayoutManager(context)
            viewAdapter = PostsAdapter(posts)

            recyclerView = rootView.posts_recycler_view.apply {
                layoutManager = viewManager
                adapter = viewAdapter
            }

            //Restore items on rotation
            if (savedInstanceState != null) {
                mainViewModel.postsSequential.value?.let {
                    viewAdapter.setItems(it)
                }
                mainViewModel.postsParallel.value?.let {
                    viewAdapter.setItems(it)
                }
            }
        }

        //Observe loading status
        mainViewModel.isLoadingInProgress.observe(this, observerLoadingProgress)

        //Observe execution time
        mainViewModel.executionTime.observe(this, observerExecutionTime)

        rootView.fab_sequential.setOnClickListener {
            //Init observer for posts
            mainViewModel.postsSequential.observe(this, observerSequentialPosts)
            // Manual load posts
            mainViewModel.loadPosts()
        }

        rootView.fab_paralel.setOnClickListener {
            //Init observer for posts
            if (!mainViewModel.postsParallel.hasActiveObservers()) {
                mainViewModel.postsParallel.observe(this, observerParallelPosts)
            } else {
                mainViewModel.isLoadingInProgress.value = false
                mainViewModel.postsParallel.removeObserver(observerParallelPosts)
            }
        }

        return rootView
    }
}
