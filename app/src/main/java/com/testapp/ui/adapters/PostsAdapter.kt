package com.testapp.ui.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.testapp.domain.models.DomainPost
import kotlinx.android.synthetic.main.item_post.view.*
import android.widget.TextView
import android.graphics.Typeface
import android.widget.LinearLayout


class PostsAdapter(private var postsDataSet: MutableList<DomainPost>) :
    RecyclerView.Adapter<PostsAdapter.PostsViewHolder>() {

    class PostsViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val tvPostName = view.tv_post_name
        val tvPostBody = view.tv_post_body
        val llComments = view.ll_comments
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): PostsAdapter.PostsViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(com.testapp.R.layout.item_post, parent, false) as View
        return PostsViewHolder(view)
    }

    override fun onBindViewHolder(holder: PostsViewHolder, position: Int) {
        holder.tvPostName.text = postsDataSet[position].title
        holder.tvPostBody.text = postsDataSet[position].body

        for ((i, comment) in postsDataSet[position].comments.withIndex()) {

            val tvCommentUsername = TextView(holder.llComments.context)
            tvCommentUsername.id = i
            tvCommentUsername.text = comment.name
            tvCommentUsername.setTypeface(null, Typeface.BOLD_ITALIC)
            tvCommentUsername.layoutParams = LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT
            )

            val tvCommentBody = TextView(holder.llComments.context)
            tvCommentBody.id = i
            tvCommentBody.text = " - " + comment.body
            tvCommentBody.layoutParams = LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT
            )

            holder.llComments.addView(tvCommentUsername)
            holder.llComments.addView(tvCommentBody)
        }

    }

    override fun getItemCount() = postsDataSet.size

    fun setItems(items: List<DomainPost>) {
        postsDataSet.clear()
        postsDataSet.addAll(items)
        notifyDataSetChanged()
    }
}