package com.testapp.db

import android.content.Context
import androidx.room.Room
import org.koin.dsl.module

val dbModule = module {
    single { provideDB(get()) }
    single { get<AppDatabase>().userDao() }
}

fun provideDB(applicationContext: Context): AppDatabase {
    return Room.databaseBuilder(
        applicationContext,
        AppDatabase::class.java, "testDB"
    ).build()
}
