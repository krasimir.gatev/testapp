package com.testapp

import android.app.Application
import com.testapp.db.dbModule
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin
import com.testapp.networking.networkModule
import com.testapp.repository.forumModule
import com.testapp.ui.main.mainViewModelModule

class TestApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidLogger()
            androidContext(this@TestApplication)
            modules(listOf(networkModule, dbModule, forumModule, mainViewModelModule))
        }
    }
}