package com.testapp.repository

import com.testapp.db.UserDao
import com.testapp.domain.models.DomainComment
import com.testapp.domain.models.DomainPost
import com.testapp.networking.TestAppServices
import com.testapp.networking.models.WsComment
import com.testapp.networking.models.WsPost
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.async
import org.koin.dsl.module
import kotlin.coroutines.coroutineContext

val forumModule = module {
    factory { ForumRepository(get(), get())}
}

class ForumRepository(private val serviceApi: TestAppServices, private val userDao: UserDao) {

    //Sequential Execution
    suspend fun getDomainPostsSequential(): List<DomainPost>{

        //get userId from DB
        val userId = userDao.getLoggedUser()?.serverId ?: return listOf()

        //call get posts
        val posts = serviceApi.getPosts(userId)

        //call get comments
        val comments = serviceApi.getComments(userId)

        return mapToDomainPosts(posts, comments)
    }

    //Parallel Execution
    suspend fun getDomainPostsParallel(): List<DomainPost>{

        //get userId from DB
        val userId = userDao.getLoggedUser()?.serverId ?: return listOf()

        //call get posts with default coroutineContext. Can add additional attributes by coroutineContext + context
        val posts = CoroutineScope(coroutineContext).async {serviceApi.getPosts(userId)}

        //call get comments
        val comments = CoroutineScope(coroutineContext).async {serviceApi.getComments(userId)}

        return mapToDomainPosts(posts.await(), comments.await())
    }

    private fun mapToDomainPosts(posts: List<WsPost>, comments: List<WsComment>): List<DomainPost>{
        val domainPosts = mutableListOf<DomainPost>()
        for (post in posts) {
            val commentsList = mapToDomainComments(comments).filter { it.postId == post.id }
            domainPosts.add(DomainPost(commentsList, post.userId, post.id, post.title, post.body))
        }
        return domainPosts
    }

   private fun mapToDomainComments(comments: List<WsComment>): List<DomainComment>{
        val domainComments = mutableListOf<DomainComment>()
        for (comment in comments) {
            domainComments.add(DomainComment(comment.postId, comment.id, comment.name, comment.email, comment.body))
        }
        return domainComments
    }
}